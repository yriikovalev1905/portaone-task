
const formText = document.getElementById('iputString').value;
const button = document.getElementById('btn');
const result = document.getElementById('output');


button.addEventListener('click', onResult);

function onResult(event) {
    event.preventDefault();

    const stringText = formText;

    const arrOfWords = stringText.replace(/[^a-zA-Z]/gi, " ")
    .replace(/ {1,}/g, " ")
    .trim()
    .toLowerCase()
    .split(' ');

const firtsNonReapitingLetters = [];

for (const word of arrOfWords) {
    const firstLetter = word.slice(1, arrOfWords.length);
    if (!firstLetter.includes(word[0])) {
        firtsNonReapitingLetters.push(word[0]);
    }
    
}

const getUniqLetters = arr => 
    arr.filter((item,index) => {
        arr.splice(index, 1);
        const uniqueArr = !arr.includes(item);
        arr.splice(index, 0, item);
        return uniqueArr;
    })

const uniqueLetter = getUniqLetters(firtsNonReapitingLetters).slice(0, 1).join('');


    result.innerText = uniqueLetter;

}







